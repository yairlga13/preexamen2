const breedSelect = document.getElementById('breed-select');
const loadBreedBtn = document.getElementById('load-breed');
const dogImage = document.getElementById('dog-image');
const viewImageBtn = document.getElementById('view-image');

let isImageLoaded = false; // Variable para verificar si se ha cargado una imagen

function loadImage(selectedBreed) {
    if (!selectedBreed) {
        console.error('No breed selected.');
        return;
    }

    axios.get(`https://dog.ceo/api/breed/${selectedBreed}/images/random`)
        .then(response => {
            const imageUrl = response.data.message;
            dogImage.src = imageUrl;
            isImageLoaded = true; // Marcar como imagen cargada
            viewImageBtn.disabled = false; // Habilitar el botón "Ver imagen"
        })
        .catch(error => console.error('Error loading image:', error));
}

function loadBreeds() {
    breedSelect.innerHTML = ''; // Limpiar las opciones existentes en el select
    axios.get('https://dog.ceo/api/breeds/list')
        .then(response => {
            const breeds = response.data.message;
            breeds.forEach(breed => {
                const option = document.createElement('option');
                option.text = breed;
                breedSelect.add(option);
            });
        })
        .catch(error => console.error('Error fetching breeds:', error));
}

loadBreedBtn.addEventListener('click', () => {
    loadBreeds(); // Cargar las razas al hacer clic en "Cargar Raza"
});

viewImageBtn.addEventListener('click', () => {
    const selectedBreed = breedSelect.value;
    loadImage(selectedBreed); // Cargar la imagen de la raza seleccionada al hacer clic en "Ver imagen"
});
